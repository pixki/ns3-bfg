/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "ns3/core-module.h"
#include "ns3/bfg-helper.h"
#include "ns3/bfg-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wifi-module.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet.h"

//NetAnim functionality
#include "ns3/netanim-module.h"

using namespace ns3;

/**
 * @brief The BFGExample class creates a 1-dimensional grid topology
 */
class BFGExample{
public:
    BFGExample();
    bool Configure(int argc, char **argv);
    void Run();
    void Report(std::ostream &os);

private:
    //number of nodes in the grid
    uint32_t size;
    //Distance between nodes, in meters
    double step;
    //Simulation time in seconds
    double totalTime;
    //Write pcap traces per device if true
    bool pcapTrace;    
    //Seed for RNG
    uint32_t seed;

    //Network
    NodeContainer nodes;
    NetDeviceContainer devices;
    Ipv4InterfaceContainer interfaces;


    //Methods to create and run the simulation
    void CreateNodes();
    void CreateDevices();
    void InstallInternetStack();
    void InstallApplications();
};



int main (int argc, char *argv[])
{
    //Log all levels
    LogComponentEnable("BFGRoutingProtocol", LOG_LEVEL_ALL);
    //Each line will print [Time] [Node] [Function] [Level] [Message]
    //Use LOG_PREFIX_TIME or any variant to control the output
    LogComponentEnable ("BFGRoutingProtocol", LOG_PREFIX_ALL);

    //LogComponentEnable("PacketMetadata", LOG_LEVEL_ALL);
    //LogComponentEnable("Ipv4L3Protocol", LOG_LEVEL_ALL);
    LogComponentEnable ("UdpSocketImpl", LOG_LEVEL_ALL);

    BFGExample ex;
    if(!ex.Configure(argc, argv))
        NS_FATAL_ERROR("Configuration failed. Aborted.");

    ex.Run();
    //ex.Report();
    return 0;
}



BFGExample::BFGExample() :
    size(3),
    step(100),
    totalTime(10),
    pcapTrace(true),
    seed(1)
{
}


bool
BFGExample::Configure(int argc, char **argv){

    SeedManager::SetSeed(12345);
    CommandLine cmd;
    cmd.AddValue ("pcap", "Write PCAP traces.", pcapTrace);
    cmd.AddValue ("size", "Number of nodes.", size);
    cmd.AddValue ("time", "Simulation time, s.", totalTime);
    cmd.AddValue ("step", "Grid step, m", step);
    cmd.AddValue ("seed", "Seed for pseudo-random number generation.", seed);

    cmd.Parse (argc, argv);
    return true;
}


void
BFGExample::Run ()
{
    //Enable printing on packets
    Packet::EnablePrinting ();
    Packet::EnableChecking ();

    CreateNodes ();
    CreateDevices ();
    InstallInternetStack ();
    InstallApplications ();

    Simulator::Stop (Seconds (totalTime));

    AnimationInterface anim ("bfg-animation.xml");
    anim.UpdateNodeDescription (nodes.Get (0), "Source");
    anim.UpdateNodeColor (nodes.Get (0), 255, 0, 0);
    anim.EnablePacketMetadata (true); // Optional
    anim.EnableIpv4RouteTracking ("bfgroutingtable-wireless.xml", Seconds(0), Seconds(10), Seconds(0.25));
    anim.EnableWifiMacCounters (Seconds (0), Seconds (10)); //Optional
    anim.EnableWifiPhyCounters (Seconds (0), Seconds (10)); //Optional

    /*Should be congruent to the mobility/position specified in CreateNodes*/
    for(uint32_t i=0; i<size; i++)
    {
      std::cout<<"Asignando posicion al nodo "<< i <<std::endl;
      anim.SetConstantPosition (nodes.Get (i), i*100, 0);
    }
    Simulator::Run ();
    Simulator::Destroy ();
}





void
BFGExample::Report (std::ostream &os)
{
}

void
BFGExample::CreateNodes ()
{
    std::cout << "Creating " << (unsigned)size << " nodes " << step << " m apart.\n";
    nodes.Create (size);
    // Name nodes
    for (uint32_t i = 0; i < size; ++i) {
        std::ostringstream os;
        os << "node-" << i;
        Names::Add (os.str (), nodes.Get (i));
    }

    // Create static grid
    MobilityHelper mobility;
    mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                   "MinX", DoubleValue (0.0),
                                   "MinY", DoubleValue (0.0),
                                   "DeltaX", DoubleValue (step),
                                   "DeltaY", DoubleValue (0),
                                   "GridWidth", UintegerValue (size),
                                   "LayoutType", StringValue ("RowFirst"));
    mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
    mobility.Install (nodes);
}


void
BFGExample::CreateDevices ()
{
    NqosWifiMacHelper wifiMac = NqosWifiMacHelper::Default ();
    wifiMac.SetType ("ns3::AdhocWifiMac");
    YansWifiPhyHelper wifiPhy = YansWifiPhyHelper::Default ();
    YansWifiChannelHelper wifiChannel = YansWifiChannelHelper::Default ();
    wifiPhy.SetChannel (wifiChannel.Create ());
    WifiHelper wifi = WifiHelper::Default ();
    wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager", "DataMode", StringValue ("OfdmRate6Mbps"), "RtsCtsThreshold", UintegerValue (0));
    devices = wifi.Install (wifiPhy, wifiMac, nodes);

    if (pcapTrace)
    {
        wifiPhy.EnablePcapAll (std::string ("bfg"));
    }
}



void
BFGExample::InstallInternetStack ()
{
    BFGHelper bfg;
    // you can configure BFG attributes here using bfg.Set(name, value)
    std::cout << "Seed for RNG: "<< seed << std::endl;
    bfg.Set ("Seed", UintegerValue(seed));

    InternetStackHelper stack;
    stack.SetRoutingHelper (bfg); // has effect on the next Install ()
    stack.Install (nodes);
    Ipv4AddressHelper address;
    address.SetBase ("10.0.0.0", "255.0.0.0");
    interfaces = address.Assign (devices);
}

void
BFGExample::InstallApplications ()
{
    ApplicationContainer cbrApps;
    uint16_t sinkPort = 12345;
    Ipv4Address sinkAddress = interfaces.GetAddress (size - 1);

    OnOffHelper cbrHelper("ns3::UdpSocketFactory", InetSocketAddress(sinkAddress, sinkPort));
    cbrHelper.SetConstantRate(DataRate("200Bps"), 200);// 1 packet of 200 bytes each second
    cbrHelper.SetAttribute("StartTime", TimeValue(Seconds(3.0)));
    cbrApps.Add(cbrHelper.Install(nodes.Get(0)));

    // move node away
    //Ptr<Node> node = nodes.Get (size/2);
    //Ptr<MobilityModel> mob = node->GetObject<MobilityModel> ();
    //Simulator::Schedule (Seconds (totalTime/3), &MobilityModel::SetPosition, mob, Vector (1e5, 1e5, 1e5));
}




