#include "ns3/bfg-helper.h"
#include "ns3/bfg-module.h"
#include <cstdio>

#define SUVSIZE 5

using namespace ns3;


InternalBuffer buffer(2048);


uint32_t GetDestination(){
  static uint32_t copies = 6;
  static uint32_t out = 512;
  if( copies-- == 0 ) {
      copies = 10;
      out *= 2;
  }
  printf("GetDestination  %d", out);
  return out;
}


void PopulateBuffer(uint32_t size){
  uint32_t seq = 0;
  uint32_t remaining = size;

  while(remaining--){
      PacketIdentifier id;
      id.dst_ = GetDestination();
      id.dst_port_ = 8192;
      id.src_ = 0x00000001;
      id.src_port_ = 1000;
      id.seq_num_ = seq++;
      id.size_ = 200;

      uint8_t* data = new uint8_t[200];

      buffer.Add (id, data, 200);
      printf("Identificador agregado [%d]\n", buffer.Size ());
    }
}


void SendSUV(std::list<PacketIdentifier> resume){
  printf("--SendSUV %ld identificadores\n", resume.size ());
}

void PrintList(std::list<PacketIdentifier> theList){
  std::list<PacketIdentifier>::const_iterator it;
  uint32_t index=0;
  std::cout << "================================================"<< std::endl;

  for(it=theList.begin (); it != theList.end (); ++it){
      PacketIdentifier id = *it;
      std::cout<< "[" << index++ << "] " <<id.src_ <<":"<<id.src_port_ <<" --> ";
      std::cout<<id.dst_<<":"<<id.dst_port_ << "("<< id.seq_num_ <<")"<< std::endl;
    }
  std::cout << "================================================"<< std::endl<<std::endl;
}


void
ReceiveBloomFilter(){
    double Prj, Pri = 0.0;
    list<PacketIdentifier> resume;
    std::vector<Ipv4Address>::const_iterator destIterator;
    std::vector<Ipv4Address> destinations = buffer.GetDestinations ();

    printf("Hay %ld destinos distintos.\n", destinations.size ());

    buffer.Print (std::cout);

    for(destIterator = destinations.begin (); destIterator != destinations.end(); ++destIterator){
        Ipv4Address destination = *destIterator;
        Prj = 0.5;
        Pri = 0.3;

        std::cout << "Iteracion de destino, hay "<< resume.size () << " paquetes sin enviar"<<std::endl;

        if(Prj >= Pri || Prj == 1.0){
            //Add to the resume all the packets that we have for that destination
            std::list<PacketIdentifier> temp = buffer.GetPacketsFor (destination);
            std::cout << "j es viable para este destino " << Ipv4Address(destination) << std::endl;
            PrintList (resume);

            resume.merge (temp);

            printf("Despues del merge\n");
            PrintList (resume);

            std::cout << "  Ahora hay "<< resume.size () << " ids por enviar"<< std::endl;

            while(resume.size() >= SUVSIZE){
                printf("Enviando resumen con %d identificadores...\n", SUVSIZE);
                std::list<PacketIdentifier> sublist;
                uint32_t rpackets = SUVSIZE;
                sublist.insert (sublist.end (), SUVSIZE, resume.front ());
                while(rpackets){
                    resume.pop_front ();
                    rpackets--;
                  }
                SendSUV(sublist);
                printf("Después del SendSUV\n");
                PrintList (resume);
                sublist.clear();
            }
        }
    }

    printf("Restantes\n");
    PrintList (resume);
    if(resume.size() > 0){
        SendSUV(resume);
        resume.clear();
    }
    printf("Final\n");
    PrintList (resume);
}





int main (int argc, char *argv[]){

  PopulateBuffer (13);
  printf("Buffer llenado\n");
  ReceiveBloomFilter ();

}
