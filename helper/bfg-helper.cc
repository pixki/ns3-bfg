/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "bfg-helper.h"

namespace ns3 {

BFGHelper::BFGHelper() :
    Ipv4RoutingHelper()
{
    m_agentFactory.SetTypeId("ns3::bfg::BFGRoutingProtocol");
}

BFGHelper*
BFGHelper::Copy() const{
    return new BFGHelper(*this);
}



Ptr<Ipv4RoutingProtocol>
BFGHelper::Create(Ptr<Node> node) const{
    Ptr<bfg::BFGRoutingProtocol> agent = m_agentFactory.Create<bfg::BFGRoutingProtocol>();
    node->AggregateObject(agent);
    return agent;
}


void
BFGHelper::Set(std::string name, const AttributeValue &value){
    m_agentFactory.Set(name, value);
}


int64_t
BFGHelper::AssignStreams(NodeContainer c, int64_t stream){
    int64_t currentStream = stream;
    Ptr<Node> node;
    for (NodeContainer::Iterator i = c.Begin (); i != c.End (); ++i)
    {
        node = (*i);
        Ptr<Ipv4> ipv4 = node->GetObject<Ipv4> ();
        NS_ASSERT_MSG (ipv4, "Ipv4 not installed on node");
        Ptr<Ipv4RoutingProtocol> proto = ipv4->GetRoutingProtocol ();
        NS_ASSERT_MSG (proto, "Ipv4 routing not installed on node");
        Ptr<bfg::BFGRoutingProtocol> bfg = DynamicCast<bfg::BFGRoutingProtocol> (proto);
        if (bfg)
        {
            currentStream += bfg->AssignStreams (currentStream);
            continue;
        }
        // BFG may also be in a list
        Ptr<Ipv4ListRouting> list = DynamicCast<Ipv4ListRouting> (proto);
        if (list)
        {
            int16_t priority;
            Ptr<Ipv4RoutingProtocol> listProto;
            Ptr<bfg::BFGRoutingProtocol> listBFG;
            for (uint32_t i = 0; i < list->GetNRoutingProtocols (); i++)
            {
                listProto = list->GetRoutingProtocol (i, priority);
                listBFG = DynamicCast<bfg::BFGRoutingProtocol> (listProto);
                if (listBFG)
                {
                    currentStream += listBFG->AssignStreams (currentStream);
                    break;
                }
            }
        }
    }
    return (currentStream - stream);
}

}

