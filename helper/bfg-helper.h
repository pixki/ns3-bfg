/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef BFG_HELPER_H
#define BFG_HELPER_H

#include "ns3/bfg.h"
#include "ns3/ipv4-routing-helper.h"
#include "ns3/object-factory.h"
#include "ns3/node-container.h"
#include "ns3/node.h"

namespace ns3 {

class BFGHelper : public Ipv4RoutingHelper{
public:
    BFGHelper();

    /**
     * @brief Copy This method is mainly for internal use by the other helpers;
     *   clients are expected to free the dynamic memory allocated by this method
     * @return pointer to clone of this BFGHelper
     */
    BFGHelper* Copy() const;

    /**
     * @brief Create This method will be called by ns3::InternetStackHelper::Install
     * @param node the node on which the routing protocol will run
     * @return A newly created routing protocol
     */
    virtual Ptr<Ipv4RoutingProtocol> Create(Ptr<Node> node) const;

    /**
     * @brief Set This method controls the attributes of ns3::bfg::BFGRoutingProtocol
     * @param name the name of the attribute to set
     * @param value the value of the attribute to set
     */
    void Set(std::string name, const AttributeValue &value);

    /**
     * @brief AssignStreams Assign a fixed random variable stream number to the random variables
     * used by this model. Return the number of streams (possibly zero) that have been assigned.
     * The Install() method of the InternetStackHelper should have previously been called by the
     * user.
     * @param c NodeContainer of the set of nodes for which BFG should be modified to use a
     *          fixed stream.
     * @param stream first stream index to use
     * @return the number of streams indices assigned by this helper
     */
    int64_t AssignStreams(NodeContainer c, int64_t stream);

private:
    //The factory used to create BFG routing object
    ObjectFactory m_agentFactory;
};

}

#endif /* BFG_HELPER_H */

