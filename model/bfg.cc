/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */

#include "bfg.h"
#include "ns3/log.h"
#include "bfgheader.h"
#include "ns3/boolean.h"
#include "ns3/inet-socket-address.h"
#include "ns3/udp-socket-factory.h"
#include "ns3/integer.h"
#include "ns3/pointer.h"
#include "ns3/string.h"
#include "ns3/random-variable-stream.h"
#include "ns3/uinteger.h"
#include "ns3/udp-header.h"
#include "time.h"

NS_LOG_COMPONENT_DEFINE("BFGRoutingProtocol");


namespace ns3 {
namespace bfg {


NS_OBJECT_ENSURE_REGISTERED(BFGRoutingProtocol);

const uint16_t BFGRoutingProtocol::BFG_PORT = 1234;
//uint32_t BFGRoutingProtocol::m_seed   = 1L;  //Default seed

TypeId
BFGRoutingProtocol::GetTypeId(){
    static TypeId tid = TypeId("ns3::bfg::BFGRoutingProtocol")
            .SetParent<Ipv4RoutingProtocol>()
            .AddConstructor<BFGRoutingProtocol>()
            .AddAttribute("HelloInterval",                 //name
                          "Hello messages interval",       //help
                          TimeValue(Seconds(1)),           //Default value
                          MakeTimeAccessor(&BFGRoutingProtocol::HelloInterval), //Accessed through
                          MakeTimeChecker())               //Checker is used to set bounds to values
            .AddAttribute ("DegradationInterval",
                           "Degradation time interval for internal Bloom filter",
                           TimeValue(Seconds(4)),
                           MakeTimeAccessor(&BFGRoutingProtocol::p_degradationInterval),
                           MakeTimeChecker ())
            .AddAttribute("BufferSize",
                          "Number of packets that any node can store.",
                          UintegerValue(2048),
                          MakeUintegerAccessor(&BFGRoutingProtocol::p_bufferSize),
                          MakeUintegerChecker<uint32_t>())
            .AddAttribute("Seed",
                          "Seed used in pseudo-random numbers",
                          UintegerValue(100),
                          MakeUintegerAccessor(&BFGRoutingProtocol::Seed),
                          MakeUintegerChecker<uint32_t>())
            .AddAttribute("UniformRV",
                          "Access to the underlying random variable",
                          StringValue("ns3::UniformRandomVariable"),
                          MakePointerAccessor(&BFGRoutingProtocol::m_URandom),
                          MakePointerChecker<UniformRandomVariable> ());
    return tid;
}


/**
 * @brief BFGRoutingProtocol::BFGRoutingProtocol The constructor, the values declared as default here are merely placeholders
 * as they will be eventually replaced by the defaults and/or values specified through the object factories. See available
 * Attributes in GetTypeId
 */
BFGRoutingProtocol::BFGRoutingProtocol() :
    HelloInterval(Seconds(1)),
    m_htimer(Timer::CANCEL_ON_DESTROY),
    p_degradationInterval(Seconds (1)),
    m_degTimer(Timer::CANCEL_ON_DESTROY),
    filtersInitialized(false),
    buffer_(Parameters::BufferSize)
{
    //The filters are empty at this time because no address has been given yet to the node
}


BFGRoutingProtocol::~BFGRoutingProtocol(){

}


void
BFGRoutingProtocol::DoDispose(){
    m_ipv4 = 0;
    //Close every raw socket in the node (one per interface)
    for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::iterator iter =
         m_socketAddresses.begin (); iter != m_socketAddresses.end (); iter++)
        iter->first->Close ();

    m_socketAddresses.clear ();
    Ipv4RoutingProtocol::DoDispose ();
}


/**
 *Outgoing packets arrive at this function to acquire a route, we can still return a null route and intercept the packet
 */
Ptr<Ipv4Route>
BFGRoutingProtocol::RouteOutput(Ptr<Packet> p, const Ipv4Header &header, Ptr<NetDevice> oif, Socket::SocketErrno &sockerr){
    NS_LOG_FUNCTION("Packet from upper layer: "<< p->GetUid() << header.GetDestination() );

    if( !p ){
        sockerr = Socket::ERROR_INVAL;
        NS_LOG_WARN("Tried to send a NULL Packet");
        Ptr<Ipv4Route> route;
        return route;
    }


    if( m_socketAddresses.empty() ){
        sockerr = Socket::ERROR_NOROUTETOHOST;
        NS_LOG_WARN("No BFG interfaces");
        Ptr<Ipv4Route> route;
        return route;
    }

    //The interface with index 0 is loopback
    int32_t ifIndex = oif ? m_ipv4->GetInterfaceForDevice(oif) : 1;
    Ipv4Address firstAddress = m_ipv4->GetAddress (0, 0).GetLocal ();
    if( ifIndex < 0 ){
        sockerr = Socket::ERROR_NOROUTETOHOST;
        NS_LOG_LOGIC ("Could not get the address for this NetDevice");
        Ptr<Ipv4Route> route;
        return route;
    }else if( m_ipv4->GetNInterfaces () <= 1 && firstAddress == Ipv4Address("127.0.0.1")){
        NS_LOG_ERROR("Loopback interface is the only configured interface");
        sockerr = Socket::ERROR_ADDRNOTAVAIL;
        Ptr<Ipv4Route> route;
        return route;
    }



    Ipv4InterfaceAddress iface = m_ipv4->GetAddress(ifIndex, 0);
    sockerr = Socket::ERROR_NOTERROR;
    Ipv4Address dst = header.GetDestination ();

    //Instead of returning a route to the packet, we intercept the packet and store it inside the buffer.

    NS_ASSERT( header.GetProtocol() == 0x11);//Only deal with UDP Packets for the moment

    Ptr<Packet> packet = p->Copy();
    UdpHeader udp;
    packet->PeekHeader (udp);
    //udp.Print(std::cout);
    NS_LOG_DEBUG("Dst IP: "<< dst << "  Src IP: " << iface.GetLocal ());
    NS_LOG_DEBUG("Src port:" << udp.GetSourcePort() << " Dst port:" << udp.GetDestinationPort());
    //packet->GetSize() returns the bytes carried by the packet (8 bytes of UDP Header + the bytes specified as data)
    NS_LOG_DEBUG("Packet UID: " << packet->GetUid () << " Size: " << packet->GetSize () );

    uint32_t bytes = packet->GetSize ();    
    uint8_t *buffer = new uint8_t[bytes];
    memset(buffer, 0, bytes);
    packet->CopyData (buffer, bytes);
    //PrintHexBuffer (buffer, bytes, "PACKET CONTENTS");
    PacketIdentifier newPacket = CreatePacketIdentifier (packet);
    this->buffer_.Add (newPacket, buffer, bytes);


    Ptr<Ipv4Route> route = NULL;

    //Construct a route object to return
    //Ptr<Ipv4Route> route = Create<Ipv4Route>();
    //Ptr<NetDevice> dev = m_ipv4->GetNetDevice(ifIndex);
    //route->SetDestination (dst);
    //route->SetGateway (iface.GetLocal ());//nextHop, iface.GetBroadcast()
    //route->SetSource (iface.GetLocal ());
    //route->SetOutputDevice(dev);
    return route;
}


/**
 *Incoming packets on this node arrive here, eventually one of the provided callbacks are used to further process the packet
 */
bool
BFGRoutingProtocol::RouteInput(Ptr<const Packet> p, const Ipv4Header &header, Ptr<const NetDevice> idev, UnicastForwardCallback ufcb, MulticastForwardCallback mfcb, LocalDeliverCallback ldcb, ErrorCallback errcb){

    NS_LOG_FUNCTION ("Packet received: [uid:" << p->GetUid () <<"dst:"<< header.GetDestination ()<<"]");

    if(m_socketAddresses.empty ()){//No interface is listening
        NS_LOG_LOGIC ("No listening interfaces");
        return false;
    }

    NS_ASSERT (m_ipv4 != 0);                               //The IPv4 Stack is running in this node
    NS_ASSERT (p != 0);                                    //The packet is not null
    int32_t iif = m_ipv4->GetInterfaceForDevice (idev);    //Get the interface index
    NS_ASSERT (iif >= 0);                                  //The input device also supports IPv4


    Ipv4Address dst = header.GetDestination ();
    Ipv4Address src = header.GetSource ();

    // Duplicate of own packet
    if (IsMyOwnAddress (src)){
        NS_LOG_DEBUG("Packet from myself (Origin address "<< src << ")");
        //return true;
    }

    if(dst.IsMulticast ()) {//Deal with the multicast packet
        NS_LOG_INFO ("Multicast destination...");
        //mfcb(p,header,iif);
    }

    //Broadcast, local delivery or forwarding
    for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j) {

        Ipv4InterfaceAddress iface = j->second;

        if(m_ipv4->GetInterfaceForAddress (iface.GetLocal ()) == iif) {//We got the interface that received the packet

            NS_LOG_DEBUG("Got the interface which received the packet");
            if(dst == iface.GetBroadcast () || dst.IsBroadcast ()) {//...and it's a broadcasted packet

                NS_LOG_DEBUG("The destination is the broadcast address.");

                Ptr<Packet> packet = p->Copy ();
                if(  ! ldcb.IsNull () ) {//Forward the packet to further processing to the LocalDeliveryCallback defined
                    NS_LOG_DEBUG("Forwarding packet to Local Delivery Callback");
                    ldcb(packet,header,iif);
                } else {
                    NS_LOG_ERROR("Unable to deliver packet: LocalDeliverCallback is null.");
                    errcb(packet,header,Socket::ERROR_NOROUTETOHOST);
                }

                if (header.GetTtl () > 1) {
                    NS_LOG_LOGIC ("Forward broadcast...");
                    //Get a route and call UnicastForwardCallback
                } else {
                    NS_LOG_LOGIC ("TTL Exceeded, drop packet");
                }

                return true;
            }else{
                NS_LOG_DEBUG("Received a packet ["<<iface.GetLocal()<<"] with destination [" << dst);
            }
        }else{
            NS_LOG_DEBUG("{"<< iface.GetLocal() <<"} The indexes mismatched: " << m_ipv4->GetInterfaceForAddress(iface.GetLocal()) << " =/=" << iif);
        }
    }



    //Unicast local delivery
    if( m_ipv4->IsDestinationAddress (dst, iif)) {
        if (ldcb.IsNull () == false) {
            NS_LOG_LOGIC ("Unicast local delivery to " << dst);
            ldcb (p, header, iif);
        } else {
            NS_LOG_ERROR ("Unable to deliver packet locally due to null callback " << p->GetUid () << " from " << src);
            errcb (p, header, Socket::ERROR_NOROUTETOHOST);
        }
        return true;
    }


    //Forwarding
    return Forwarding (p,header,ufcb,errcb);

}


bool
BFGRoutingProtocol::IsMyOwnAddress (Ipv4Address addr){
  for (std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j =
        m_socketAddresses.begin (); j != m_socketAddresses.end (); ++j)
  {
     Ipv4InterfaceAddress iface = j->second;
     if (addr == iface.GetLocal ())
     {
        return true;
     }
  }

  return false;
}


/**
 * @brief BFGRoutingProtocol::SetIpv4 Asigna el stack de protocolo IPv4 al nodo, nótese que el default solamente
 *        implementa la interfaz loopback. Para lógica del protocolo con las diferentes direcciones IP veáse
 *        NotifyAddAddress y NotifyInterfaceUp
 * @param ipv4
 */
void
BFGRoutingProtocol::SetIpv4(Ptr<Ipv4> ipv4){
    NS_ASSERT( ipv4 != 0 );
    NS_ASSERT( m_ipv4 == 0 );

    m_ipv4 = ipv4;

    //Configure HelloTimer, will be scheduled in Start function
    m_htimer.SetFunction( &BFGRoutingProtocol::HelloTimerExpire, this);    
    m_degTimer.SetFunction( &BFGRoutingProtocol::DoDegradation, this);


    //At this point we can create the filters accordingly to the values passed by the factories
    //Simulator::ScheduleNow(&BFGRoutingProtocol::Start, this);
}


void
BFGRoutingProtocol::NotifyInterfaceUp(uint32_t interface){
    NS_LOG_DEBUG("Interface lista " << m_ipv4->GetAddress(interface, 0).GetLocal());

    if( !filtersInitialized ){
        Start(); //Initialize here instead of SetIpv4 because of the use of the assigned IP
        NS_LOG_DEBUG("Inicializando los filtros");
        m_bloomFilterStar->add(m_ipv4->GetAddress (interface, 0).GetLocal ());
        m_bloomFilterTime->add(m_ipv4->GetAddress (interface, 0).GetLocal ());
        NS_LOG_DEBUG("Soy "<< m_ipv4->GetAddress(interface, 0).GetLocal ());
        NS_LOG_DEBUG("   FB=" << this->m_bloomFilterStar->to_string ());
        // filtersInitialized = true;

    }else{
      NS_LOG_WARN("Bloom filter already initialized. Ignoring interface");
    }




    Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol>();
    if( l3->GetNAddresses(interface) > 1){
        NS_LOG_WARN ("More than one address assigned in interface");
    }

    Ipv4InterfaceAddress iface = l3->GetAddress(interface, 0);
    if (iface.GetLocal() == Ipv4Address("127.0.0.1"))
        return;

    //Create a Socket to listen only in this interface
    Ptr<Socket> socket = Socket::CreateSocket(GetObject<Node>(), UdpSocketFactory::GetTypeId());
    NS_ASSERT( socket != 0 );
    socket->SetRecvCallback(MakeCallback(&BFGRoutingProtocol::RecvBFG, this));
    socket->Bind(InetSocketAddress(Ipv4Address::GetAny(), BFG_PORT));
    socket->BindToNetDevice(l3->GetNetDevice(interface));
    socket->SetAllowBroadcast(true);
    socket->SetAttribute("IpTtl", UintegerValue(1));

    m_socketAddresses.insert(std::make_pair( socket, iface));
}



void
BFGRoutingProtocol::NotifyInterfaceDown(uint32_t interface){
    NS_LOG_DEBUG("Interfaz removida" << this << m_ipv4->GetAddress(interface, 0).GetLocal());

    //Close socket
    Ptr<Socket> socket = FindSocketWithInterfaceAddress(m_ipv4->GetAddress(interface, 0));
    NS_ASSERT (socket); //Ensure that the socket exists

    socket->Close();
    m_socketAddresses.erase(socket);
    if( m_socketAddresses.empty() ){
        NS_LOG_LOGIC( "No BFG interfaces" );
        //Suspend the timer as there is no more listening sockets
        m_htimer.Cancel();
        return;
    }
}


void
BFGRoutingProtocol::NotifyAddAddress(uint32_t interface, Ipv4InterfaceAddress address){
    NS_LOG_DEBUG( "Direccion agregada interface " << interface << " direccion " << address);
    Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol>();

    if( !l3->IsUp(interface) ) //The interface is down
        return;

    if( l3->GetNAddresses(interface) == 1 ){
        Ipv4InterfaceAddress iface = l3->GetAddress(interface, 0);
        Ptr<Socket> socket = FindSocketWithInterfaceAddress(iface);
        if( !socket ){
            if( iface.GetLocal() == Ipv4Address("127.0.0.1") )
                return;

            //Create a socket to listen only in this interface
            Ptr<Socket> skt = Socket::CreateSocket(GetObject<Node>(), UdpSocketFactory::GetTypeId());
            NS_ASSERT( skt != 0 );
            skt->SetRecvCallback(MakeCallback(&BFGRoutingProtocol::RecvBFG, this));
            skt->BindToNetDevice(l3->GetNetDevice(interface));
            //Bind to any IP Address so that broadcasts can be received.
            skt->Bind(InetSocketAddress(Ipv4Address::GetAny(), BFG_PORT));
            skt->SetAllowBroadcast(true);
            m_socketAddresses.insert(std::make_pair(skt, iface));
        }
    }else{
        NS_LOG_LOGIC("BFG does not work with more than one address per each interface. Ignoring added address");
    }
}


void
BFGRoutingProtocol::NotifyRemoveAddress(uint32_t interface, Ipv4InterfaceAddress address){
    NS_LOG_DEBUG("Direccion removida:" << this << " interface "<< interface << " address " << address);

    Ptr<Socket> socket = FindSocketWithInterfaceAddress(address);

    if( socket ){ //If the socket exists, we should close it
        m_socketAddresses.erase(socket);
        Ptr<Ipv4L3Protocol> l3 = m_ipv4->GetObject<Ipv4L3Protocol>();

        if( l3->GetNAddresses(interface) ){//Exists at least one address
            Ipv4InterfaceAddress iface = l3->GetAddress(interface, 0);

            //Create a socket in the next Address available
            Ptr<Socket> skt = Socket::CreateSocket(GetObject<Node>(), UdpSocketFactory::GetTypeId());
            NS_ASSERT( skt != 0 );
            skt->SetRecvCallback(MakeCallback (&BFGRoutingProtocol::RecvBFG, this));
            //Bind to any IP address so we can receive broacasts
            skt->Bind(InetSocketAddress( Ipv4Address::GetAny(), BFG_PORT));
            skt->SetAllowBroadcast(true);
            m_socketAddresses.insert(std::make_pair(skt, iface));
        }

        if( m_socketAddresses.empty() ){
            NS_LOG_LOGIC( "No BFG interface" );
            m_htimer.Cancel();
            return;
        }
    }else{
        NS_LOG_LOGIC( "Remove address nor participating in BFG operation" );
    }
}


void
BFGRoutingProtocol::PrintRoutingTable(Ptr<OutputStreamWrapper> stream) const{
    *stream->GetStream() << "(" << m_ipv4->GetObject<Node>()->GetId() << " - Not implemented yet";
}


int64_t
BFGRoutingProtocol::AssignStreams(int64_t stream){
    //NS_LOG_FUNCTION( this << stream );
    m_URandom->SetStream(stream);
    return 1;
}

/**
 * @brief BFGRoutingProtocol::Start Initialize data and other variables not initialized in the constructor.
 * This method should take the values set by the Attributes defined in GetTypeId
 */
void
BFGRoutingProtocol::Start(){
    NS_LOG_INFO( "Iniciando operacion de protocolo BFG" );
    NS_LOG_DEBUG("Seed: "<< this->Seed);
    NS_LOG_DEBUG("Dt: " << this->p_degradationInterval);

    CountingFilter::set_seed (this->Seed);
    this->m_bloomFilterStar = new CountingFilter(Parameters::BF_Counters, Parameters::BF_HashFunctions, Parameters::BF_MaxValue);
    this->m_bloomFilterTime = new CountingFilter(Parameters::BF_Counters, Parameters::BF_HashFunctions, Parameters::BF_MaxValue);

    this->buffer_.SetSize (this->p_bufferSize);

    //Initialize timers and extra behaviour not initialized in the constructor
    m_htimer.Schedule(BFGRoutingProtocol::HelloInterval);
    m_degTimer.Schedule (BFGRoutingProtocol::p_degradationInterval);
}



void
BFGRoutingProtocol::HelloTimerExpire(){
    NS_LOG_DEBUG( "Sending Hello" );

    if(filtersInitialized) //Send only when everything is ready
      SendHello();

    m_htimer.Cancel();
    m_htimer.Schedule(BFGRoutingProtocol::HelloInterval);
}


void
BFGRoutingProtocol::DoDegradation (){
  NS_LOG_DEBUG("Degradation timer expired");


  if(filtersInitialized){
    this->m_bloomFilterTime->degrada (Parameters::DegradationProbability);
    *this->m_bloomFilterTime += *this->m_bloomFilterStar;
  }

  m_degTimer.Cancel ();
  m_degTimer.Schedule (BFGRoutingProtocol::p_degradationInterval);
}


bool
BFGRoutingProtocol::Forwarding(Ptr<const Packet> p, const Ipv4Header &header, UnicastForwardCallback ufcb, ErrorCallback errcb){
    //Not forwarding packets for the moment
    return false;
}


/**
* Broadcast a HELLO packet
*/
void
BFGRoutingProtocol::SendHello(){

    for( std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin(); j != m_socketAddresses.end(); ++j){
        Ptr<Socket> socket = j->first;
        Ipv4InterfaceAddress iface = j->second;


        BloomFilterHeader bloomHeader(*m_bloomFilterTime);

        NS_LOG_DEBUG("Sending Bloom Filter...");

        Ptr<Packet> packet = Create<Packet>();
        packet->AddHeader(bloomHeader);

        BFGHeader bfgHeader(BFGHeader::PKT_BLOOMF);
        bfgHeader.SetDestAddress(Ipv4Address("255.255.255.255"));
        bfgHeader.SetSequenceNumber(this->m_sequenceNumber++);
        bfgHeader.SetSourceAddress(iface.GetLocal());
        packet->AddHeader(bfgHeader);

        //Send to all-hosts broadcast if on /32 addr, subnet-directed otherwise
        Ipv4Address destination;
        if(iface.GetMask() == Ipv4Mask::GetOnes()){
            destination = Ipv4Address("255.255.255.255");
        }else
            destination = iface.GetBroadcast();

        Time jitter = Time(MilliSeconds(m_URandom->GetInteger(0,100))); //Jitter between 0 and 100ms
        Simulator::Schedule(jitter, &BFGRoutingProtocol::SendTo, this, socket, packet, destination);
    }

}


/**
 * @brief BFGRoutingProtocol::RecvBFG Callback for receiving BFG packets
 * @param socket The receiving socket
 */
void
BFGRoutingProtocol::RecvBFG(Ptr<Socket> socket){
    Address sourceAddress;
    //Read a single packet from 'socket' and retrieve the 'sourceAddress'
    Ptr<Packet> packet = socket->RecvFrom(sourceAddress);

    InetSocketAddress inetSourceAddress = InetSocketAddress::ConvertFrom(sourceAddress);
    Ipv4Address  sender = inetSourceAddress.GetIpv4();
    Ipv4Address  receiver = m_socketAddresses[socket].GetLocal();

    NS_LOG_DEBUG("Sender:   " << sender);
    NS_LOG_DEBUG("Receiver: " << receiver);

    BFGHeader bHeader;
    packet->RemoveHeader(bHeader);

    PacketType pt = bHeader.GetPacketType();

    CreatePacketIdentifier(packet);

    if( pt == BFGHeader::PKT_BLOOMF ){
        NS_LOG_DEBUG("Recvd BloomFilter");
        BloomFilterHeader hdr;
        packet->RemoveHeader (hdr);
        ReceiveBloomFilter (sender, hdr.GetBloomFilter ());

    }else if( pt== BFGHeader::PKT_SUV ){
        NS_LOG_DEBUG("Recvd SummaryVector");

    }else if( pt==BFGHeader::PKT_REQ ){
        NS_LOG_DEBUG("Recvd Request");

    }else if( pt==BFGHeader::PKT_DAT ){
        NS_LOG_DEBUG("Recvd Data");

    }else{
        NS_LOG_WARN("Invalid packet type");
    }

    //Do the actual processing
}


/**
 * @brief BFGRoutingProtocol::FindSocketWithInterfaceAddress
 * @param iface
 * @return
 */
Ptr<Socket>
BFGRoutingProtocol::FindSocketWithInterfaceAddress(Ipv4InterfaceAddress addr) const{
    NS_LOG_FUNCTION(this << addr);
    //Iterate through all the configured sockets and/or interfaces until we found the right socket
    for(std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j=m_socketAddresses.begin();
        j != m_socketAddresses.end(); ++j){

        Ptr<Socket> socket = j->first;
        Ipv4InterfaceAddress iface = j->second;
        if( iface == addr){
            return socket;
        }
    }
    Ptr<Socket> sock;
    return sock;
}


/**
 * @brief BFGRoutingProtocol::SendTo
 * @param socket
 * @param packet
 * @param destination
 */
void
BFGRoutingProtocol::SendTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination){
    socket->SendTo(packet, 0, InetSocketAddress(destination, BFG_PORT));
}

//____________________________________________________________________________________________________________
void
BFGRoutingProtocol::ReceiveBloomFilter(Ipv4Address src, CountingFilter fj){
    double Prj, Pri = 0.0;
    list<PacketIdentifier> resume;
    std::vector<Ipv4Address>::const_iterator destIterator;
    std::vector<Ipv4Address> destinations = buffer_.GetDestinations ();

    for(destIterator = destinations.begin (); destIterator != destinations.end(); ++destIterator){
        Ipv4Address destination = *destIterator;
        Prj = CalculateProbabilityThrough(destination, fj);
        Pri = CalculateProbabilityThrough(destination, *m_bloomFilterTime);

        if(Prj >= Pri + Parameters::ForwardThreshold || Prj == 1.0){
            //Add to the resume all the packets that we have for that destination
            std::list<PacketIdentifier> temp = buffer_.GetPacketsFor (destination);
            resume.merge (temp);

            if(resume.size() >= Parameters::Hdr_PacketsInSUV){
                NS_LOG_DEBUG("Sending SUV to "<<src);
                std::list<PacketIdentifier> sublist;
                uint32_t rpackets = Parameters::Hdr_PacketsInSUV;
                sublist.insert (sublist.end (), Parameters::Hdr_PacketsInSUV, resume.front ());
                while(rpackets){
                    resume.pop_front ();
                    rpackets--;
                  }
                SendSUV(src, sublist);
                resume.clear();
                sublist.clear();
            }
        }
    }


    if(resume.size() > 0){
        SendSUV(src, resume);
        resume.clear();
    }

    UpdateBloomFilter(fj);
}


void
BFGRoutingProtocol::SendSUV(Ipv4Address dest, list<PacketIdentifier> resume) {
    for( std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin(); j != m_socketAddresses.end(); ++j){
        Ptr<Socket> socket = j->first;
        Ipv4InterfaceAddress iface = j->second;

        SummaryVectorHeader suvHeader;
        suvHeader.SetSummaryVector(resume);
        suvHeader.SetQuantity(resume.size());

        NS_LOG_DEBUG("Sending Summary Vector...");

        Ptr<Packet> packet = Create<Packet>();
        packet->AddHeader(suvHeader);

        BFGHeader bfgHeader(BFGHeader::PKT_SUV);
        bfgHeader.SetDestAddress(dest);
        bfgHeader.SetSequenceNumber(this->m_sequenceNumber++);
        bfgHeader.SetSourceAddress(iface.GetLocal());
        packet->AddHeader(bfgHeader);

        Time jitter = Time(MilliSeconds(m_URandom->GetInteger(0,100))); //Jitter between 0 and 100ms
        Simulator::Schedule(jitter, &BFGRoutingProtocol::SendTo, this, socket, packet, dest);
    }
}

void
BFGRoutingProtocol::ReceiveSUV(Ipv4Address src, Ptr<Packet> p){
    SummaryVectorHeader suvHeader;
    p->RemoveHeader(suvHeader);
    std::list<PacketIdentifier> request;
    std::list<PacketIdentifier> suv = suvHeader.GetSummaryVector();

    std::list<PacketIdentifier>::const_iterator it;
    for(it=suv.begin(); it != suv.end(); ++it){
        if( !HasBeenSeen(*it) ){
            request.push_back(*it);
        }
    }

    if(request.size() > 0){
        SendRequest(src, request);
    }
}


void
BFGRoutingProtocol::SendRequest(Ipv4Address dest, list<PacketIdentifier> requestedPackets){
    for( std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin(); j != m_socketAddresses.end(); ++j){
        Ptr<Socket> socket = j->first;
        Ipv4InterfaceAddress iface = j->second;


        SummaryVectorHeader reqHeader;
        reqHeader.SetSummaryVector(requestedPackets);
        reqHeader.SetQuantity(requestedPackets.size());

        NS_LOG_DEBUG("Sending Request Vector...");

        Ptr<Packet> packet = Create<Packet>();
        packet->AddHeader(reqHeader);

        BFGHeader bfgHeader(BFGHeader::PKT_REQ);
        bfgHeader.SetDestAddress(dest);
        bfgHeader.SetSequenceNumber(this->m_sequenceNumber++);
        bfgHeader.SetSourceAddress(iface.GetLocal());
        packet->AddHeader(bfgHeader);

        Time jitter = Time(MilliSeconds(m_URandom->GetInteger(0,100))); //Jitter between 0 and 100ms
        Simulator::Schedule(jitter, &BFGRoutingProtocol::SendTo, this, socket, packet, dest);
    }
}


void
BFGRoutingProtocol::ReceiveRequest(Ipv4Address src, Ptr<Packet> p){
    //Iterate through the received list and send each requested packet
    SummaryVectorHeader requestHeader;
    Ptr<Packet> packet = p->Copy();
    packet->RemoveHeader(requestHeader);

    NS_ASSERT(requestHeader.GetQuantity() > 0);
    NS_ASSERT(requestHeader.GetQuantity() <= Parameters::Hdr_PacketsInSUV);

    std::list<PacketIdentifier> requested = requestHeader.GetSummaryVector();
    std::list<PacketIdentifier>::const_iterator it;

    for(it=requested.begin(); it != requested.end(); ++it){
        SendDataPacket(*it, src);
    }
}


void
BFGRoutingProtocol::SendDataPacket(PacketIdentifier pi, Ipv4Address dest){
    for( std::map<Ptr<Socket>, Ipv4InterfaceAddress>::const_iterator j = m_socketAddresses.begin(); j != m_socketAddresses.end(); ++j){
        Ptr<Socket> socket = j->first;
        Ipv4InterfaceAddress iface = j->second;

        DataHeader dataHeader;
        char const* dt = "12334567890";
        dataHeader.SetData((uint8_t*) dt, 10);
        NS_LOG_DEBUG("Sending Data Packet...");

        Ptr<Packet> packet = Create<Packet>();
        packet->AddHeader(dataHeader);

        BFGHeader bfgHeader(BFGHeader::PKT_DAT);
        bfgHeader.SetDestAddress(dest);
        bfgHeader.SetSequenceNumber(this->m_sequenceNumber++);
        bfgHeader.SetSourceAddress(iface.GetLocal());
        packet->AddHeader(bfgHeader);

        Time jitter = Time(MilliSeconds(m_URandom->GetInteger(0,100))); //Jitter between 0 and 100ms
        Simulator::Schedule(jitter, &BFGRoutingProtocol::SendTo, this, socket, packet, dest);
    }
}


//____________________________________________________________________________________________________________

double
BFGRoutingProtocol::CalculateProbabilityThrough(PacketIdentifier pi, CountingFilter f){
    double pr=0.0;
    std::vector<uint32_t> indexes = f.hash_values_for(pi.dst_);

    double sum = 0.0;
    for(std::vector<uint32_t>::const_iterator it = indexes.begin(); it != indexes.end(); ++it){
        sum += f.get_counter_at(*it);
    }
    pr = sum / (Parameters::BF_HashFunctions * Parameters::BF_MaxValue * 1.0);
    return pr;
}

double
BFGRoutingProtocol::CalculateProbabilityThrough(Ipv4Address dst, CountingFilter f){
    double pr=0.0;
    std::vector<uint32_t> indexes = f.hash_values_for(dst);

    double sum = 0.0;
    for(std::vector<uint32_t>::const_iterator it = indexes.begin(); it != indexes.end(); ++it){
        sum += f.get_counter_at(*it);
    }
    pr = sum / (Parameters::BF_HashFunctions * Parameters::BF_MaxValue * 1.0);
    return pr;
}



void
BFGRoutingProtocol::UpdateBloomFilter(CountingFilter fj){
    NS_LOG_DEBUG("Updating internal Bloom Filter");
    NS_LOG_DEBUG("     Fj =" << fj.to_string ());
    NS_LOG_DEBUG("     FiT=" << this->m_bloomFilterTime->to_string ());
    fj.degrada(Parameters::DegradationProbability);
    *this->m_bloomFilterTime += fj;
    NS_LOG_DEBUG(" Fi(T+1)=" << this->m_bloomFilterTime->to_string ());
}




//____________________________________________________________________________________________________________
void
BFGRoutingProtocol::AddToCache(PacketIdentifier pi){
    this->cache_.insert(pi);
}


bool
BFGRoutingProtocol::HasBeenSeen(PacketIdentifier pi){
    const bool is_in_cache = cache_.find(pi) != cache_.end();
    return is_in_cache;
}


void
BFGRoutingProtocol::PrintBloomFilter(ostream &os){

}

PacketIdentifier
BFGRoutingProtocol::CreatePacketIdentifier(Ptr<Packet> p){
    PacketIdentifier pi;
    Ptr<Packet> packet = p->Copy();
    std::cout << "------------- Printing PacketTags ----------"<<std::endl;
    packet->PrintPacketTags(std::cout);
    std::cout << std::endl;
    std::cout << "------------- ------------------- ----------"<<std::endl;

    std::cout << "------------- Printing ByteTags ------------"<<std::endl;
    packet->PrintByteTags(std::cout);
    std::cout << std::endl;
    std::cout << "------------- ------------------- ----------"<<std::endl;


    return pi;
}


void
BFGRoutingProtocol::PrintHexBuffer (const uint8_t *buffer, uint32_t size, const string message){
  uint32_t i, restante = size, imp, inicio, fin;
  printf("_%p_ %s - [DEBUG] %d bytes \n ", buffer, message.c_str (), size);
  imp = size < 16 ? size : 16;
  inicio = 0, fin = imp;

  printf("          00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F \n");
  printf("---------------------------------------------------------- \n");
  while (restante) {
    printf("[%08X] ", inicio);
    for (i = 0; i < imp; i++) {
      printf("%02X ", *(buffer + i + inicio));
    }
    printf(" | ");

    for (i = 0; i < imp; i++) {
      printf("%c ", *(buffer + i + inicio));
    }

    printf("\n");

    inicio += imp, fin += imp;
    imp = restante < 16 ? restante : 16;
    restante -= imp;
  }

  printf("%s - [/DEBUG] \n  ", message.c_str ());
}


}}

