/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
#ifndef BFG_H
#define BFG_H

#include "ns3/packet.h"
#include "ns3/ipv4-routing-protocol.h"
#include "ns3/ipv4-l3-protocol.h"
#include "ns3/node.h"
#include "ns3/timer.h"
#include "ns3/random-variable-stream.h"

#include "bloomfilter.h"
#include "bfgheader.h"
#include "internalbuffer.h"


#include <map>


using namespace ns3::bfg;

namespace ns3 {
namespace bfg {


class BFGRoutingProtocol : public Ipv4RoutingProtocol {
public:
    static const uint16_t BFG_PORT; //Defined in bfg.cc
    static TypeId GetTypeId(void);

    BFGRoutingProtocol();
    virtual ~BFGRoutingProtocol();
    virtual void DoDispose();

    //From Ipv4RoutingProtocol
    Ptr<Ipv4Route> RouteOutput(Ptr<Packet> p,
                               const Ipv4Header &header,
                               Ptr<NetDevice> oif,
                               Socket::SocketErrno &sockerr);
    bool RouteInput(Ptr<const Packet> p,
                    const Ipv4Header &header,
                    Ptr<const NetDevice> idev,
                    UnicastForwardCallback ucb,
                    MulticastForwardCallback mcb,
                    LocalDeliverCallback ldcb,
                    ErrorCallback errcb);

    virtual void SetIpv4(Ptr<Ipv4> ipv4);
    virtual void NotifyInterfaceUp (uint32_t interface);
    virtual void NotifyInterfaceDown (uint32_t interface);
    virtual void NotifyAddAddress (uint32_t interface, Ipv4InterfaceAddress address);
    virtual void NotifyRemoveAddress (uint32_t interface, Ipv4InterfaceAddress address);
    virtual void PrintRoutingTable (Ptr<OutputStreamWrapper> stream) const;

    int64_t AssignStreams(int64_t stream);


private:
    //Start protocol operation
    void        Start();
    void        SendTo   (Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination);
    void        RecvBFG(Ptr<Socket> socket);
    Ptr<Socket> FindSocketWithInterfaceAddress (Ipv4InterfaceAddress addr) const;
    //In case there exists a route to the destination, the packet is forwarded
    bool        Forwarding(Ptr<const Packet> p, const Ipv4Header &header, UnicastForwardCallback ufcb, ErrorCallback errcb);
    void        SendUnicastTo(Ptr<Socket> socket, Ptr<Packet> packet, Ipv4Address destination);
    bool        IsMyOwnAddress(Ipv4Address addr);

    //HELLO intervals and timers
    Time   HelloInterval;
    Timer  m_htimer;
    void   SendHello();
    void   HelloTimerExpire();

    Time   p_degradationInterval;
    Timer  m_degTimer;
    void   DoDegradation();


    //IPv4 Protocol
    Ptr<Ipv4>   m_ipv4;
    uint16_t    m_sequenceNumber;
    // Raw socket per each IP interface, map socket -> iface address (IP + mask)
    std::map< Ptr<Socket>, Ipv4InterfaceAddress > m_socketAddresses;

    //Used to simulate jitter
    Ptr<UniformRandomVariable> m_URandom;

    uint32_t Seed;

    uint32_t p_bufferSize;


    //The Bloom Filters
    CountingFilter* m_bloomFilterStar; //The Bloom Filter with only the local address added to it
    CountingFilter* m_bloomFilterTime; //The Bloom Filter that gets updated on each contact

    bool filtersInitialized;

    /*
     * Functions to deal with packets of this protocol
     */
    void	ReceiveBloomFilter(Ipv4Address src, CountingFilter fj);

    void    SendSUV(Ipv4Address dest, list<PacketIdentifier> resume);
    void    ReceiveSUV(Ipv4Address src, Ptr<Packet> p);

    void    SendRequest(Ipv4Address dest, list<PacketIdentifier> requestedPackets);
    void    ReceiveRequest(Ipv4Address src, Ptr<Packet> p);

    void    SendDataPacket(PacketIdentifier pi, Ipv4Address dest);

    /* Utility functions */
    void    PrintHexBuffer(const uint8_t *buffer, uint32_t size, const std::string message);

    /**
     * @brief CalculateProbabilityThrough Calculates the probability of reaching the destination with the information
     *        contained in the CountingFilter
     * @param pi The PacketIdentifier with the destination for which we want to know the probability
     * @param f The CountingFilter with the network information
     * @return The probability of reaching the destination
     */
    double  CalculateProbabilityThrough(PacketIdentifier pi, CountingFilter f);
    double  CalculateProbabilityThrough(Ipv4Address ip, CountingFilter f);


    /**
     * @brief UpdateBloomFilter Merges the information between a filter and the internal Bloom filter of this node
     * @param fj The other Bloom Filter
     */
    void    UpdateBloomFilter(CountingFilter fj);

    /**
     * @brief buffer_ The internal data structure to store the intermediate packets
     */
    InternalBuffer buffer_;

    /**
     * @brief cache_ Stores the PacketIdentifier's of every packet that this node has 'seen'
     */
    set<PacketIdentifier> cache_;

    /**
     * @brief AddToCache Adds the packet to the cache
     * @param pi The PacketIdentifier for the packet to add
     */
    void AddToCache(PacketIdentifier pi);

    /**
     * @brief HasBeenSeen Tests if a packet exists in the internal cache
     * @param pi The PacketIdentifier of the packet to test
     * @return true if packet is already in cache
     */
    bool HasBeenSeen(PacketIdentifier pi);

    /**
     * @brief AddToBuffer Adds a packet to the buffer, invoked when this protocol receives a packet from upper layer
     * @param p The packet to insert
     */
    void AddNewToBuffer(Ptr<Packet> p);


    /**
     * @brief PrintBloomFilter Outputs a human readable representation of the internal Bloom Filter (m_bloomFilterTime)
     * @param os The outuput stream to which the output will be directed.
     */
    void PrintBloomFilter(std::ostream &os);


    /**
     * @brief CreatePacketIdentifier Creates a PacketIdentifier object for a certain packet
     * @param p The packet for which the PacketIdentifier will be created
     * @return The PacketIdentifier
     */
    PacketIdentifier CreatePacketIdentifier(Ptr<Packet> p);

};

}}

#endif /* BFG_H */

