#include "bfgheader.h"
#include "ns3/packet.h"
#include "ns3/address-utils.h"
#include "ns3/fatal-error.h"
#include "ns3/log.h"



namespace ns3 {
namespace bfg {

NS_OBJECT_ENSURE_REGISTERED(BFGHeader);
NS_LOG_COMPONENT_DEFINE ("BFGHeader");


uint32_t Parameters::BF_HashFunctions = 6;
uint32_t Parameters::BF_Counters      = 32;
uint32_t Parameters::BF_MaxValue      = 32;
uint32_t Parameters::Hdr_PacketsInSUV = 20;
uint32_t Parameters::Hdr_DataBytes    = Parameters::Hdr_PacketsInSUV*sizeof(PacketIdentifier);
uint32_t Parameters::BufferSize       = 2048;
double Parameters::ForwardThreshold = 0.2;
double Parameters::DegradationProbability= 1.0;

BFGHeader::BFGHeader(): type(1){

}

BFGHeader::BFGHeader(PacketType t){

    if(t == BFGHeader::PKT_BLOOMF ||
       t == BFGHeader::PKT_SUV ||
       t == BFGHeader::PKT_REQ ||
       t == BFGHeader::PKT_DAT){

        this->type = t;
    }else{
        NS_FATAL_ERROR("The specified PacketType is illegal.");
    }
}


TypeId
BFGHeader::GetTypeId()
{
    static TypeId tid = TypeId("ns3::bfg::BFGHeader")
            .SetParent<Header>()
            .AddConstructor<BFGHeader>();
    return tid;
}

TypeId
BFGHeader::GetInstanceTypeId() const
{
    return GetTypeId();
}

uint32_t
BFGHeader::GetSerializedSize() const
{
    return 12; //Total number of bytes when serialized
}



void
BFGHeader::Serialize(Buffer::Iterator start) const
{
    uint8_t buf[4];
    //One way to write an address into a header
    this->source.Serialize(buf);
    start.Write(buf, 4);

    //Alternatively we can write it this other way
    WriteTo(start, this->dest);

    start.WriteHtonU16(this->sequenceNumber);

    start.WriteU8(this->type);

    start.WriteU8(this->padding);

}

/**
 * @brief BFGHeader::Deserialize
 * @param start an iterator which points to where the header should
 *              read from.
 * @return The number of bytes read
 */
uint32_t
BFGHeader::Deserialize(Buffer::Iterator start)
{
    Buffer::Iterator i = start;

    //Get the source address
    ReadFrom(i, this->source);

    //Get the dest address
    ReadFrom(i, this->dest);

    //Get all other fields
    this->sequenceNumber = i.ReadNtohU16();
    this->type = i.ReadU8();
    this->padding = i.ReadU8();

    //Validate that the readed bytes match the serialized size
    uint32_t dist = i.GetDistanceFrom(start);
    NS_ASSERT( dist == GetSerializedSize() );

    return dist;
}

void
BFGHeader::Print(std::ostream &os) const
{
    os << source << " sends";
    switch(type){
    case BFGHeader::PKT_BLOOMF:
         os<<" BloomFilter ";
         break;
    case BFGHeader::PKT_DAT:
        os<<" Data packet ";
        break;
    case BFGHeader::PKT_REQ:
        os<<" Request ";
        break;
    case BFGHeader::PKT_SUV:
        os<<" Summary vector ";
        break;
    }
    os << "to " << dest << "\n";
}


/********************************************************************
 * BloomFilterHeader methods
 ********************************************************************
*/

BloomFilterHeader::BloomFilterHeader() : filter(1,1,1){
    //Using a placeholder filter, need to use SetBloomFilter
}

BloomFilterHeader::BloomFilterHeader(CountingFilter f) : filter(f){

}

TypeId
BloomFilterHeader::GetTypeId(){
    static TypeId tid = TypeId("ns3::bfg::BloomFilterHeader")
            .SetParent<Header>()
            .AddConstructor<BloomFilterHeader>();
    return tid;
}

TypeId
BloomFilterHeader::GetInstanceTypeId() const {
    return GetTypeId();
}


uint32_t
BloomFilterHeader::GetSerializedSize() const{
    return filter.bytes_needed(Parameters::BF_Counters, Parameters::BF_MaxValue);
}

void
BloomFilterHeader::Serialize(Buffer::Iterator start) const{
    //Serialize the BloomFilter
    uint8_t* f = (uint8_t *) this->filter.serialize();
    start.Write(f, filter.bytes_needed());
}

uint32_t
BloomFilterHeader::Deserialize(Buffer::Iterator start){
    Buffer::Iterator i = start;

    uint32_t bytesToRead = filter.bytes_needed(Parameters::BF_Counters, Parameters::BF_MaxValue);
    uint8_t data[bytesToRead];

    i.Read(data, bytesToRead);

    CountingFilter* recvd = CountingFilter::deserialize(data, bytesToRead, Parameters::BF_Counters, Parameters::BF_HashFunctions, Parameters::BF_MaxValue);
    this->filter = *recvd;

    //Validate that the readed bytes match the serialized size
    uint32_t dist = i.GetDistanceFrom(start);
    NS_ASSERT( dist == GetSerializedSize() );

    return dist;
}

void
BloomFilterHeader::Print(ostream &os) const{
    os << this->filter.to_string()<< "\n";
}

/********************************************************************
 * SummaryVectorHeader methods
 ********************************************************************
*/
SummaryVectorHeader::SummaryVectorHeader(){
    this->suv = new PacketIdentifier [Parameters::Hdr_PacketsInSUV];
}

SummaryVectorHeader::~SummaryVectorHeader(){
    if( this->suv != NULL )
        delete[] suv;
}


void
SummaryVectorHeader::SetSummaryVector(std::list<PacketIdentifier> suv){
    std::list<PacketIdentifier>::const_iterator it;

    this->suv = new PacketIdentifier[suv.size()];

    int index=0;
    for(it=suv.begin(); it != suv.end(); ++it, index++){
        this->suv[index] = *it;
    }
}


std::list<PacketIdentifier>
SummaryVectorHeader::GetSummaryVector(){    
    list<PacketIdentifier> theList;
    for(int i=0; i<this->quantity; i++){
        theList.push_back(suv[i]);
    }
    return theList;
}


TypeId
SummaryVectorHeader::GetTypeId(){
    static TypeId tid = TypeId("ns3::bfg::SummaryVectorHeader")
            .SetParent<Header>()
            .AddConstructor<SummaryVectorHeader>();
    return tid;
}

TypeId
SummaryVectorHeader::GetInstanceTypeId() const {
    return GetTypeId();
}

uint32_t
SummaryVectorHeader::GetSerializedSize() const{
    //Size of the packets plus the extra size byte
    return (Parameters::Hdr_PacketsInSUV * this->quantity + 1);
}

void
SummaryVectorHeader::Serialize(Buffer::Iterator start) const{

    start.WriteU8(quantity);

    //Access the underlying bytes to the array of structs    
    uint8_t* suvBytes = reinterpret_cast<uint8_t*>(this->suv);
    start.Write(suvBytes, sizeof(this->suv));    
}

uint32_t
SummaryVectorHeader::Deserialize(Buffer::Iterator start){
    Buffer::Iterator i = start;

    quantity = i.ReadU8();

    uint8_t suvBytes[quantity];
    i.Read(suvBytes, sizeof(suvBytes));

    //Validate that the readed bytes match the serialized size
    uint32_t dist = i.GetDistanceFrom(start);
    NS_ASSERT( dist == GetSerializedSize() );
    return dist;
}


void
SummaryVectorHeader::Print(ostream &os) const{
    os << "Summary Vector\n";
}

/********************************************************************
 * DataHeader methods
 ********************************************************************
*/
DataHeader::DataHeader(){
    this->data = new uint8_t[Parameters::Hdr_DataBytes];
}

DataHeader::~DataHeader(){
    if( this->data != NULL ){
        delete data;
    }
}

TypeId
DataHeader::GetTypeId(){
    static TypeId tid = TypeId("ns3::bfg::DataHeader")
            .SetParent<Header>()
            .AddConstructor<DataHeader>();
    return tid;
}

TypeId
DataHeader::GetInstanceTypeId() const {
    return GetTypeId();
}

uint32_t
DataHeader::GetSerializedSize() const {
    return Parameters::Hdr_DataBytes;
}

void
DataHeader::Serialize(Buffer::Iterator start) const {
    start.WriteU16(this->size);
    start.Write(this->data, this->size);
}

uint32_t
DataHeader::Deserialize(Buffer::Iterator start){
    Buffer::Iterator i = start;

    size = i.ReadU16();
    i.Read(this->data, Parameters::Hdr_DataBytes);

    //Validate that the readed bytes match the serialized size
    uint32_t dist = i.GetDistanceFrom(start);
    NS_ASSERT( dist == GetSerializedSize() );
    return dist;
}

void
DataHeader::SetData(const uint8_t *data, uint16_t size){

    NS_ASSERT( data != 0);
    this->data = new uint8_t[size];
    this->size = size;


    for(uint16_t i = 0; i<size; i++){
        this->data[i] = data[i];
    }
}



void
DataHeader::Print(ostream &os) const{
    os << "Data bytes\n";
}


std::ostream&
operator<< (std::ostream &os, BFGHeader const &h)
{
    h.Print(os);
    return os;
}

}}


