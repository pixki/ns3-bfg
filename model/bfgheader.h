#ifndef BFGHEADER_H
#define BFGHEADER_H

#include "ns3/header.h"
#include "ns3/enum.h"
#include "ns3/ipv4-address.h"
#include "ns3/buffer.h"

#include "bloomfilter.h"
#include "bfgtypes.h"


namespace ns3{
namespace bfg{



class Parameters {
public:
    static uint32_t BF_HashFunctions;
    static uint32_t BF_Counters;
    static uint32_t BF_MaxValue;
    static uint32_t Hdr_PacketsInSUV;
    static uint32_t Hdr_DataBytes;
    static uint32_t BufferSize;    
    static double   ForwardThreshold;
    static double   DegradationProbability;
};

//Namespace constants

typedef uint8_t PacketType;

/*
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                        SOURCE ADDRESS                         |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                     DESTINATION ADDRESS                       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |      SEQUENCE NUMBER        |     TYPE      |    UNUSED       |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
/**
 * @brief The BFGHeader class Base protocol header
 * This header is the base header, extensible by one of the following Headers:
 * BloomFilterHeader, SummaryVectorHeader, DataHeader based on the value Type
 */
class BFGHeader : public Header
{
public:
    BFGHeader();
    BFGHeader(PacketType type);

    static const PacketType  PKT_BLOOMF = 1;
    static const PacketType  PKT_SUV    = 2;
    static const PacketType  PKT_REQ    = 3;
    static const PacketType  PKT_DAT    = 4;

    //Serializing and deserializing
    //{
    static TypeId    GetTypeId (void);
    TypeId           GetInstanceTypeId () const;
    virtual void     Serialize (Buffer::Iterator start) const;
    virtual uint32_t Deserialize (Buffer::Iterator start);
    virtual uint32_t GetSerializedSize () const;
    virtual void     Print (std::ostream &os) const;
    //}


    //Getters and Setters
    Ipv4Address GetSourceAddress() const { return source; }
    void SetSourceAddress(Ipv4Address addr) { this->source = addr; }

    Ipv4Address GetDestAddress() const { return dest; }
    void SetDestAddress(Ipv4Address addr) { this->dest = addr; }

    uint16_t GetSequenceNumber() { return this->sequenceNumber; }
    void SetSequenceNumber(uint16_t seq) {this->sequenceNumber = seq; }

    PacketType GetPacketType() { return this->type; }
    void SetPacketType(PacketType t) { this->type = t; }


private:
    //Header fields
    Ipv4Address        source;            //The source address which transmitted this packet
    Ipv4Address        dest;              //The intended destination
    uint16_t           sequenceNumber;    //The sequence number corresponding to this packet
    PacketType         type;              //What type of packet is this
    uint8_t            padding;

};

/* 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |          BLOOM FILTER (depending on config)                   |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
class BloomFilterHeader : public Header{
public:
    BloomFilterHeader();
    BloomFilterHeader(CountingFilter f);    

    //(De)Serializing
    static TypeId    GetTypeId();
    TypeId           GetInstanceTypeId() const;
    virtual void     Serialize(Buffer::Iterator start) const;
    virtual uint32_t Deserialize(Buffer::Iterator start);
    virtual uint32_t GetSerializedSize() const;
    virtual void     Print(std::ostream &os) const;


    //Getters and Setters
    size_t GetBytesForFilter() const {return this->m_bytes; }
    CountingFilter GetBloomFilter() const { return this->filter; }
    void SetBloomFilter(CountingFilter other ) { this->filter = other; }

    //Private fields
private:
    CountingFilter filter;
    size_t m_bytes;
};





/* 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |               SUMMARY VECTOR (depending on config)            |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
class SummaryVectorHeader : public Header{
public:
    SummaryVectorHeader();
    ~SummaryVectorHeader();

    //(De)Serializing
    static TypeId    GetTypeId();
    TypeId           GetInstanceTypeId() const;
    virtual void     Serialize(Buffer::Iterator start) const;
    virtual uint32_t Deserialize(Buffer::Iterator start);
    virtual uint32_t GetSerializedSize() const;
    virtual void     Print(std::ostream &os) const;

    //Getters and Setters
    void SetSummaryVector(std::list<PacketIdentifier> suv);
    std::list<PacketIdentifier> GetSummaryVector();

    void SetQuantity(uint8_t qty){ quantity = qty; }
    uint8_t GetQuantity() { return quantity; }

//Private fields
private:
    uint8_t           quantity; //How many PacketIdentifiers there are
    PacketIdentifier* suv;
};





/* 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |           Size                |Data (depending on config)     |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
class DataHeader : public Header{
public:
    DataHeader();
    ~DataHeader();

    //(De)Serializing
    static TypeId    GetTypeId();
    TypeId           GetInstanceTypeId() const;
    virtual void     Serialize(ns3::Buffer::Iterator start) const;
    virtual uint32_t Deserialize(ns3::Buffer::Iterator start);
    virtual uint32_t GetSerializedSize() const;
    virtual void     Print(std::ostream &os) const;

    //Getters and Setters
    void SetData(const uint8_t* data, uint16_t size);

    uint16_t GetSize() const { return size; }
    uint8_t* GetData() const { return data; }

private:
    uint16_t  size;
    uint8_t*  data;
};

std::ostream & operator<< (std::ostream & os, BFGHeader const &);

}} //end namespaces
#endif // BFGHEADER_H
