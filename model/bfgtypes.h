#ifndef BFGTYPES
#define BFGTYPES


#include <stdint.h>

namespace ns3 {
  namespace bfg {

    /**
     * @brief The PacketIdentifier struct represents a tuple that identifies uniquely a
     * packet generated in any node.
     */
    struct PacketIdentifier{
        uint32_t   src_;
        uint32_t   dst_;
        uint32_t   size_;
        uint16_t   src_port_;
        uint16_t   dst_port_;
        uint16_t   seq_num_;
    };




    /**
     * @brief operator < We use this operator to establish a total relation order between any two packets
     * @param left The packet identifier of the first packet
     * @param right The PacketIdentifier of the second
     * @return true if left has a bigger preference over right
     */
    inline bool operator < (const PacketIdentifier& left, const PacketIdentifier& right){
        if(left.src_ != right.src_){
            return left.src_ < right.src_;
        }else if(left.dst_ != right.dst_){
            return left.dst_ < right.dst_;
        }else if(left.dst_port_ != right.dst_port_){
            return left.dst_port_ < right.dst_port_;
        }else if(left.src_port_ != right.src_port_){
            return left.src_port_ < right.src_port_;
        }else if(left.seq_num_ != right.seq_num_){
            return left.seq_num_ < right.seq_num_;
        }else if(left.size_ != right.size_){
            return left.size_ < right.size_;
        }else {
            //left and right both identify the same packet
            return false;
        }
    }

  }

}


#endif // BFGTYPES

