#include "internalbuffer.h"
#include <set>
#include <cstdio>

namespace ns3 {
  namespace bfg {
    InternalBuffer::InternalBuffer(uint32_t size):
      m_size(size)
    {

    }


    uint32_t
    InternalBuffer::Size(){
      return this->m_buffer.size ();
    }

    uint32_t
    InternalBuffer::SizeInBytes (){
      uint32_t totalSize = 0;
      for(BufferConstIterator iterator = m_buffer.begin (); iterator != m_buffer.end (); ++iterator){
          Payload p = iterator->second;
          totalSize += p.first;
        }
      return totalSize;
    }


    void
    InternalBuffer::Add (PacketIdentifier id, uint8_t *payload, uint32_t payloadSize){

      if( Exists(id) ) return;

      if(m_buffer.size () == (this->m_size - 1)) return;

      Payload p = std::make_pair(payloadSize, payload);
      m_buffer[id] = p;

    }


    const uint8_t*
    InternalBuffer::GetPayload (PacketIdentifier id){
      return m_buffer.find (id)->second.second;
    }


    void
    InternalBuffer::Update (PacketIdentifier id, uint8_t *payload, uint32_t payloadSize){
      if( ! Exists(id) )  return;

      Payload p = std::make_pair(payloadSize, payload);

      BufferIterator it = m_buffer.find (id); //We already know that it != m_buffer.end()
      it->second = p;
    }


    void
    InternalBuffer::Drop (PacketIdentifier id){
      if(!Exists(id)) return;
      m_buffer.erase (id);
    }


    bool
    InternalBuffer::Exists (PacketIdentifier id){
      return m_buffer.find (id) != m_buffer.end ();
    }

    void
    InternalBuffer::SetSize (uint32_t size){
      int32_t extras=this->m_buffer.size () - size;
      if(extras < 0){
          std::cerr<< __FILE__ << __LINE__ << "Buffer already bigger than new size. Dropping packets"<< std::endl;

          for(BufferIterator it=m_buffer.begin (); it != m_buffer.end (); ){
              if(extras < 0){
                  m_buffer.erase (it++);
                  extras++;
                }
              else{
                  ++it;
                }
            }
        }

      this->m_size = size;
    }


    std::vector<Ipv4Address>
    InternalBuffer::GetDestinations (){
      std::set<Ipv4Address> destinations;

      for(BufferConstIterator it = m_buffer.begin (); it != m_buffer.end (); ++it){
          PacketIdentifier current = it->first;
          destinations.insert (Ipv4Address(current.dst_));
      }
      std::vector<Ipv4Address> dsts(destinations.begin (), destinations.end ());
      return dsts;
    }


    std::list<PacketIdentifier>
    InternalBuffer::GetPacketsFor (Ipv4Address dst){
      std::list<PacketIdentifier> to_return;
      for(BufferConstIterator bci=m_buffer.begin (); bci != m_buffer.end (); ++bci){
          PacketIdentifier pi = bci->first;
          if(pi.dst_ == dst.Get ()){
              to_return.push_back (pi);
            }
        }
      return to_return;
    }

    void
    InternalBuffer::Print (std::ostream &out){
      uint32_t position = 0;
      for(BufferIterator i= m_buffer.begin (); i != m_buffer.end (); ++i){
          PacketIdentifier id = i->first;
          char* str = new char[100];
          sprintf(str, "[%d] %d:%d-->%d:%d (%d)\n", position++, id.src_, id.src_port_, id.dst_, id.dst_port_, id.seq_num_);
          out << std::string(str);
          delete str;
        }
    }

  }
}

