#ifndef INTERNALBUFFER_H
#define INTERNALBUFFER_H

#include "ns3/object.h"
#include "ns3/ipv4-address.h"

#include <map>
#include <vector>
#include "bfgtypes.h"

namespace ns3 {
  namespace bfg {


    typedef std::pair<uint32_t, uint8_t*>  Payload;
    typedef std::map<PacketIdentifier, Payload>::iterator BufferIterator;
    typedef std::map<PacketIdentifier, Payload>::const_iterator BufferConstIterator;

    class InternalBuffer : public Object
    {
    public:
      InternalBuffer(uint32_t size);

      uint32_t       Size();
      uint32_t       SizeInBytes();

      void           Add(PacketIdentifier id, uint8_t* payload, uint32_t payloadSize);
      const uint8_t* GetPayload(PacketIdentifier id);
      void           Update(PacketIdentifier id, uint8_t* payload, uint32_t payloadSize);
      void           Drop(PacketIdentifier id);

      bool           Exists(PacketIdentifier id);

      void           SetSize(uint32_t);
      void           Print(std::ostream &out);

      std::vector<Ipv4Address>      GetDestinations();
      std::list<PacketIdentifier>   GetPacketsFor(Ipv4Address dst);


    private:
      std::map<PacketIdentifier, Payload>  m_buffer;
      uint32_t                             m_size;
    };


  }
}

#endif // INTERNALBUFFER_H
