# -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

# def options(opt):
#     pass

# def configure(conf):
#     conf.check_nonfatal(header_name='stdint.h', define_name='HAVE_STDINT_H')

def build(bld):
    module = bld.create_ns3_module('bfg', ['internet', 'wifi', 'netanim'])
    module.source = [
        'model/bloomfilter.cc',
        'model/internalbuffer.cc',
        'model/bfgheader.cc',        
        'model/bfg.cc',        
        'helper/bfg-helper.cc',
        ]

    module_test = bld.create_ns3_module_test_library('bfg')
    module_test.source = [
        'test/bfg-test-suite.cc',
        ]

    headers = bld(features='ns3header')
    headers.module = 'bfg'
    headers.source = [
        'model/bfgtypes.h',
        'model/bfgheader.h',
        'model/internalbuffer.h', 
        'model/bloomfilter.h', 
        'model/bfg.h',
        'helper/bfg-helper.h',
        ]

    if bld.env.ENABLE_EXAMPLES:
        bld.recurse('examples')
        
    # Control the generation of python bindings
    bld.ns3_python_bindings()

